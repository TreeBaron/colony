﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sanctuary
{
    class PhysicalObject : Entity
    {
        public float Rotation = 0.0f;

        public Texture2D Texture;

        public float Scale = 1.0f;

        public Vector2 Position;

        public Rectangle CollisionBox;

        public bool UseDrawBox = false;

        public Rectangle DrawBox;

        public int Depth = 0;
        
        public bool Alive = true;

        public string Name;

        public Settings Settings { get; set;}

        public PhysicalObject(Settings settings, string name)
        {
            Settings = settings;
            Name = name;
        }

        public override void Draw(SpriteBatch SB)
        {
            if(this.GetType() == typeof(AI))
            {
                this.Scale = Math.Min(Settings.MaxScale,this.Scale);
            }

            if (UseDrawBox == false)
            {
                if (Settings.PixelsRotate == true)
                {
                    SB.Draw(Texture, Position, null, Color.White, Rotation, GetRelativeCenter(), Scale, SpriteEffects.None, Depth); //Draw with rotation!
                }
                else
                {
                    SB.Draw(Texture, Position, null, Color.White, 0.0f, GetRelativeCenter(), Scale, SpriteEffects.None, Depth); //Draw with rotation!
                }
            }
            else
            {
                SB.Draw(Texture, DrawBox, Color.White);
            }
        }

        public override void Update(GameTime GT)
        {

        }

        public virtual void UpdateCollisionBox()
        {
            CollisionBox = new Rectangle((int)Position.X,(int)Position.Y,(int)GetWidth(),(int)GetHeight());
        }

        public virtual float GetWidth()
        {
            return Texture.Width * Scale;
        }

        public virtual float GetHeight()
        {
            return Texture.Height * Scale;
        }

        public virtual Vector2 GetRelativeCenter()
        {
            Vector2 Center = new Vector2(0,0);
            Center.X += this.GetWidth()/2;
            Center.Y += this.GetHeight()/2;
            return Center;
        }

        public static float AngleToPosition(Vector2 Position_Start,Vector2 Position_End)
        {
            Vector2 direction = Position_Start - Position_End;
            direction.Normalize();
            float angle = (float)Math.Atan2(direction.Y, direction.X); //swapped x and y
            return angle + ((float)Math.PI);
        }

    }
}
