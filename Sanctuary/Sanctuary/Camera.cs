﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sanctuary
{
    class Camera
    {

        public Matrix transform;
        public Viewport view;
        Vector2 Center;
        Vector2 Pos;
        public float scale = 1.0f;
        public float Rotation;
        float ScaleMove = 0.01f;
        public float WantedScale = 1.0f;
                        
        public Camera(Viewport newView)
        {
            view = newView;
        }
        
        public void Update(GameTime GT, ref Vector2 CenterCameraPosition)
        {
            Pos = new Vector2((CenterCameraPosition.X - view.Width / 2f), (CenterCameraPosition.Y - view.Height / 2f));

            Center = CenterCameraPosition;

            //Rotation += 0.01f;

            transform = Matrix.CreateTranslation(new Vector3(-Center.X, -Center.Y, 0)) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(new Vector3(scale, scale, 1)) *
                Matrix.CreateTranslation(new Vector3(view.Width * 0.5f, view.Height * 0.5f, 0));
            MoveToScale();
        }

        public void MoveToScale()
        {
            if (scale < WantedScale && scale + ScaleMove < WantedScale)
            {
                scale += ScaleMove;
            }
            else if (scale > WantedScale && scale - ScaleMove > WantedScale)
            {
                scale -= ScaleMove;
            }
        }
        
    }
}

/*
if (track != null)
{
    Pos = new Vector2((track.Pos.X - view.Width / 2f),
                         (track.Pos.Y - view.Height / 2f));


    Center = new Vector2((track.Pos.X + ((track.Texture.Width / 2) * track.scale) - (view.Width / 2)),
                         (track.Pos.Y + ((track.Texture.Height / 2) * track.scale) - (view.Height / 2)));


    Settings.Update_Mouse_Scaler();
    Center = new Vector2(Center.X-((view.Width* Settings.Mouse_Movement_Scaler)/2),Center.Y-((view.Height* Settings.Mouse_Movement_Scaler)/2));
    Center = new Vector2(Center.X+((((track.Texture.Width* track.scale)*2)* Settings.Mouse_Movement_Scaler)),Center.Y);
    Center = new Vector2(Center.X, Center.Y + ((((track.Texture.Height* track.scale)/2) * Settings.Mouse_Movement_Scaler)));

    transform = Matrix.CreateScale(new Vector3(scale, scale, 0)) * Matrix.CreateTranslation(new Vector3(-Center.X* scale, -Center.Y* scale, 0));
}
/* //save this or DIE!
else if (track != null && Track_Object_In_Center == false)
{

      Pos = new Vector2((track.Pos.X - view.Width / 2f),
                         (track.Pos.Y - view.Height / 2f));


    Center = new Vector2((track.Pos.X + ((track.Texture.Width / 2) * track.scale) - (view.Width / 2)),
                         (track.Pos.Y + ((track.Texture.Height / 2) * track.scale) - (view.Height / 2)));


    Settings.Update_Mouse_Scaler();
    Center = new Vector2(Center.X-((view.Width*Settings.Mouse_Movement_Scaler)/2),Center.Y-((view.Height*Settings.Mouse_Movement_Scaler)/2));


    transform = Matrix.CreateScale(new Vector3(scale, scale, 0)) * Matrix.CreateTranslation(new Vector3(-Center.X * scale, -Center.Y * scale, 0)); 

}
*/
