﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Un4seen.BassWasapi;

namespace Sanctuary.Forms
{
    public partial class MainMenuForm : Form
    {
        public ModeNames.Mode SelectedMode { get; set; } = ModeNames.Modes.Values.First();
        public int SelectedAudioIndex = 0;

        public MainMenuForm()
        {
            InitializeComponent();
        }

        private void MainMenuForm_Load(object sender, EventArgs e)
        {
            //add visual style types...
            foreach(string description in ModeNames.Modes.Keys)
            {
                VisualStyleComboBox.Items.Add(description);
            }

            //add combobox items
            for (int i = 0; i < BassWasapi.BASS_WASAPI_GetDeviceCount(); i++)
            {
                var device = BassWasapi.BASS_WASAPI_GetDeviceInfo(i);
                if (device.IsEnabled && device.IsLoopback)
                {
                    AudioOutputComboBox.Items.Add(string.Format("{0} - {1}", i, device.name));
                }
            }

            //select default first item
            AudioOutputComboBox.SelectedIndex = 0;

        }

        private void BeginButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void AudioOutputComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var array = (AudioOutputComboBox.Items[AudioOutputComboBox.SelectedIndex] as string).Split(' ');
            int devindex = Convert.ToInt32(array[0]);
            SelectedAudioIndex = devindex;
        }

        private void VisualStyleComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedMode = ModeNames.Modes[VisualStyleComboBox.SelectedItem.ToString()];
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
