﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sanctuary.TextureGenerator
{
    class TextureGen
    {
        /// <summary>
        /// Returns a cube texture of x,y resolution and of the color specified.
        /// </summary>
        /// <param name="resx"></param>
        /// <param name="resy"></param>
        /// <param name="color"></param>
        /// <returns></returns>
        public static Bitmap GetCube(int resx, int resy,System.Drawing.Color color)
        {
            Bitmap Cube = new Bitmap(resx, resy);
            for(int y = 0; y < resy; y++)
            {
                for(int x = 0; x < resx; x++)
                {
                    Cube.SetPixel(x,y, color);
                }
            }
            return Cube;
        }

        public static Bitmap GetTextureSquare(Microsoft.Xna.Framework.Rectangle rectangle, Bitmap bitmap)
        {
            Bitmap Cube = new Bitmap(rectangle.Width,rectangle.Height);
            
            //widht first
            for(int x = rectangle.X; x < rectangle.X + rectangle.Width; x++)
            {
                //height
                for(int y = rectangle.Y; y < rectangle.Y + rectangle.Height; y++)
                {
                    System.Drawing.Color color = bitmap.GetPixel(x,y);
                    Cube.SetPixel(x - rectangle.X, y - rectangle.Y, color);
                }
            }

            return Cube;
        }

        public static Bitmap GetTextureCircle(Microsoft.Xna.Framework.Rectangle rectangle, Bitmap bitmap)
        {
            Bitmap Cube = new Bitmap(rectangle.Width, rectangle.Height);

            //widht first
            for (int x = rectangle.X; x < rectangle.X + rectangle.Width; x++)
            {
                //height
                for (int y = rectangle.Y; y < rectangle.Y + rectangle.Height; y++)
                {
                    System.Drawing.Color color = bitmap.GetPixel(x, y);
                    //circle by measuring distance to center and determine placement
                    if (Vector2.Distance(new Vector2(rectangle.Width/2,rectangle.Height/2), new Vector2((int)(x - rectangle.X), (int)(y - rectangle.Y))) < (rectangle.Width/2))
                    {
                        Cube.SetPixel(x - rectangle.X, y - rectangle.Y, color);
                    }
                    else
                    {
                        //draw blank texture
                        Cube.SetPixel(x - rectangle.X, y - rectangle.Y, System.Drawing.Color.Transparent);
                    }
                }
            }

            return Cube;
        }

        public static Bitmap GetTextureSquareFaded(Microsoft.Xna.Framework.Rectangle rectangle, Bitmap bitmap)
        {
            Bitmap Cube = new Bitmap(rectangle.Width, rectangle.Height);

            bool visiblePixels = false;

            //widht first
            for (int x = rectangle.X; x < rectangle.X + rectangle.Width; x++)
            {
                //height
                for (int y = rectangle.Y; y < rectangle.Y + rectangle.Height; y++)
                {
                    System.Drawing.Color color = bitmap.GetPixel(x, y);
                    double alphaMultiplier = Vector2.Distance(new Vector2(rectangle.Width / 2, rectangle.Height / 2), new Vector2((int)(x - rectangle.X), (int)(y - rectangle.Y))) / rectangle.Width;
                    int result = 255 - (int)(alphaMultiplier * 255);

                    //boost colors a bit
                    result += 40;
                    result = Math.Min(255, result);

                    if(color.A == 0)
                    {
                        result = 0;
                    }
                    else
                    {
                        visiblePixels = true;
                    }
                    color = System.Drawing.Color.FromArgb((int)result, color.R, color.G, color.B);
                    Cube.SetPixel(x - rectangle.X, y - rectangle.Y, color);
                }
            }


            if (visiblePixels == false)
            {
                return null;
            }


            return Cube;
        }


        public static Bitmap GetTextureCircleFaded(Microsoft.Xna.Framework.Rectangle rectangle, Bitmap bitmap)
        {
            Bitmap Cube = new Bitmap(rectangle.Width, rectangle.Height);

            bool visiblePixels = false;

            //widht first
            for (int x = rectangle.X; x < rectangle.X + rectangle.Width; x++)
            {
                //height
                for (int y = rectangle.Y; y < rectangle.Y + rectangle.Height; y++)
                {
                    System.Drawing.Color color = bitmap.GetPixel(x, y);
                    double alphaMultiplier = Vector2.Distance(new Vector2(rectangle.Width / 2, rectangle.Height / 2), new Vector2((int)(x - rectangle.X), (int)(y - rectangle.Y))) / rectangle.Width;
                    alphaMultiplier *= 2; //increase alpha levels for invisibility on edges
                    int result = 255 - (int)(alphaMultiplier * 255);
                    result = Math.Max(0, result);
                    if (color.A == 0)
                    {
                        result = 0;
                    }
                    else
                    {
                        visiblePixels = true;
                    }
                    color = System.Drawing.Color.FromArgb((int)result, color.R, color.G, color.B);
                    Cube.SetPixel(x - rectangle.X, y - rectangle.Y, color);
                }
            }

            if(visiblePixels == false)
            {
                return null;
            }

            return Cube;
        }


        public static Bitmap GetTextureCircleSingleColor(Microsoft.Xna.Framework.Rectangle rectangle, Bitmap bitmap)
        {
            Bitmap Cube = new Bitmap(rectangle.Width, rectangle.Height);
            bool MainColorSet = false;
            System.Drawing.Color MainColor = System.Drawing.Color.Transparent;

            //widht first
            for (int x = rectangle.X; x < rectangle.X + rectangle.Width; x++)
            {
                //height
                for (int y = rectangle.Y; y < rectangle.Y + rectangle.Height; y++)
                {
                    if (MainColorSet == false)
                    {
                        MainColor = bitmap.GetPixel(x, y);
                        MainColorSet = true;

                        //assume invisible...
                        if(MainColor.A == 0)
                        {
                            return null;
                        }
                    }
                    //circle by measuring distance to center and determine placement
                    if (Vector2.Distance(new Vector2(rectangle.Width / 2, rectangle.Height / 2), new Vector2((int)(x - rectangle.X), (int)(y - rectangle.Y))) < (rectangle.Width / 2))
                    {
                        Cube.SetPixel(x - rectangle.X, y - rectangle.Y, MainColor);
                    }
                    else
                    {
                        //draw blank texture
                        Cube.SetPixel(x - rectangle.X, y - rectangle.Y, System.Drawing.Color.Transparent);
                    }
                }
            }

            return Cube;
        }


        /// <summary>
        /// When given a bitmap, it converts it to a texture2d and returns it.
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        public static Texture2D ConvertBitmapToTexture(Bitmap bitmap, Settings settings)
        {
            Microsoft.Xna.Framework.Color[] GroundColors = new Microsoft.Xna.Framework.Color[bitmap.Width * bitmap.Height];
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Microsoft.Xna.Framework.Color C = new Microsoft.Xna.Framework.Color();
                    C.A = bitmap.GetPixel(x, y).A;
                    C.R = bitmap.GetPixel(x, y).R;
                    C.G = bitmap.GetPixel(x, y).G;
                    C.B = bitmap.GetPixel(x, y).B;

                    if(C.A == 0)
                    {
                        C.R = 0;
                        C.G = 0;
                        C.B = 0;
                    }

                    GroundColors[x + y * bitmap.Width] = C;
                }
            }

            Texture2D texture = new Texture2D(settings.GD, bitmap.Width, bitmap.Height);
            texture.SetData(GroundColors);

            return texture;
        }

        public static Bitmap ConvertBitmapToBlackAndWhite(Bitmap bitmap)
        {
            int rgb;
            System.Drawing.Color c;

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    c = bitmap.GetPixel(x, y);
                    if (c.A != 0)
                    {
                        rgb = (int)Math.Round(.299 * c.R + .587 * c.G + .114 * c.B);
                        bitmap.SetPixel(x, y, System.Drawing.Color.FromArgb(c.A, rgb, rgb, rgb));
                    }
                    else
                    {
                        bitmap.SetPixel(x,y,System.Drawing.Color.FromArgb(0,0,0,0));
                    }
                }
            }

            return bitmap;
        }
    }
}
