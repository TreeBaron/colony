﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary
{
    class AudioMaster
    {
        public Analyzer AudioAnalyzer { get; set; }

        /// <summary>
        /// 0-255 frequency values, 16 of them.
        /// </summary>
        public List<byte> FrequencyValues { get; set; }

        public AudioMaster(Settings settings)
        {
            AudioAnalyzer = new Analyzer(this,settings);

            AudioAnalyzer.Enable = true;

            AudioAnalyzer.DisplayEnable = true;

            FrequencyValues = new List<byte>();

        }

        public List<int> GetFrequencies()
        {
            List<int> values = new List<int>();

            for(int i = 0; i < FrequencyValues.Count; i+=4)
            {
                int total = 0;
                total += FrequencyValues[i];
                total += FrequencyValues[i+1];
                total += FrequencyValues[i+2];
                total += FrequencyValues[i+3];
                values.Add(total);
            }

            return values;
        }
    }
}
