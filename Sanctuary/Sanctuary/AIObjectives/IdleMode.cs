﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Sanctuary
{
    class IdleMode : AIObjective
    {
        public IdleMode(AI owner)
        {
            this.Owner = owner;
        }

        public override void Update(GameTime GT)
        {
            Owner.Velocity = new Vector2(0,0);
            Owner.Throttle = 0.0f;
            //Owner.WantedRotation = 0.0f;//Owner.WantedRotation - Settings.GetDegrees(1);
            //Owner.Rotation = 0.0f;
            Owner.ScaleToOne();
            Owner.RotationLogic();
            
            //Owner.Scale = 0.0f;

        }

        //idle is always completed...
        public override bool Completed()
        {
            return true;
        }

    }
}
