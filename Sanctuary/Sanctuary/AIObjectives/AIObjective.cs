﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sanctuary
{
    abstract class AIObjective
    {
        public AI Owner = null;
        
        public abstract void Update(GameTime GT);

        public abstract bool Completed();

    }
}
