﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sanctuary
{
    class MoveToPosition : AIObjective
    {
        private Vector2 Place;
        private float Distance;
        private float InitialDistance;

        public Settings Settings { get; set; }

        public MoveToPosition(Vector2 place, float distance, AI owner, Settings settings)
        {
            Settings = settings;//order
            Place = place;
            Distance = distance;
            Owner = owner;
            InitialDistance = Vector2.Distance(Place, owner.Position);
            this.Owner.Velocity = new Vector2(Settings.R.Next(-1000, 1000), Settings.R.Next(-1000, 1000));
        }

        public override void Update(GameTime GT)
        {
            if (Completed() == false)
            {
                this.Owner.Rotation = PhysicalObject.AngleToPosition(Owner.Position, Place);
                Owner.Throttle = 1.0f;
                this.Owner.MoveForward();


                /* makes particle effect
                if (Vector2.Distance(Owner.Position, Place) < 200)
                {
                    Owner.Speed = Math.Min(Owner.Speed,200);
                }
                this.Owner.Scale *= 0.95f;
                this.Owner.Velocity.Y += 10.0f;//gravity
                */
            }

        }

        public override bool Completed()
        {
            bool highVelocityAutoArrival = false;

            if (Owner.Velocity.X > Distance || Owner.Velocity.Y > Distance)
            {
                if (Vector2.Distance(Owner.Position, Place) < Distance * 3)
                {
                    highVelocityAutoArrival = true;
                }
            }

            if(Vector2.Distance(Owner.Position,Place) < Distance || Vector2.Distance(Owner.Position,Place) > Settings.ScreenHeight || highVelocityAutoArrival)
            {
                this.Owner.Velocity = new Vector2(0,0);
                this.Owner.Throttle = 0.0f;
                this.Owner.Position = Place;
                Owner.WantedRotation = 0.0f;
                this.Owner.Rotation = 0.0f;
                return true;
            }


            return false;
        }

    }
}
