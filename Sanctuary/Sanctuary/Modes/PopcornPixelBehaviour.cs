﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary
{
    /// <summary>
    /// If on, pixels once in idle mode immediately leave it and "explode" based on music volume, etc.
    /// </summary>        
    public enum PopcornPixelBehavior
    {
        Off,
        InstantPop
    };
}
