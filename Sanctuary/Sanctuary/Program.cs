﻿using System;
using Sanctuary.Forms;

namespace Sanctuary
{

    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var settingsForm = new MainMenuForm();
            if(settingsForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (var game = new Game1(settingsForm.SelectedMode,settingsForm.SelectedAudioIndex))
                {
                    game.Run();
                }
            }
        }
    }
}
