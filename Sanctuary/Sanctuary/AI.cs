﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sanctuary
{

    class AI : PhysicalObject
    {
        public Vector2 Velocity = new Vector2(0, 0);
        public readonly float TurnRate = Utility.GetDegrees(1);
        public float Speed = 600f; //best 100 or 200
        public float Throttle = 1.0f;
        float WantedRotationActual = 0.0f;
        int IdleTurnDirectionActual;
        public AIObjective CurrentObjective = null;
        public List<AIObjective> Objectives = new List<AIObjective>();
        public float ScaleMove = 0.05f;//best 0.007

        private bool m_scaleLocked = false;

        public float WantedScale = 0.1f;

        public bool CanModify = true;

        public int IdleTurnDirection { get => IdleTurnDirectionActual; set => IdleTurnDirectionActual = value; }
        public float WantedRotation { get => WantedRotationActual; set => WantedRotationActual = value; }

        public bool TurnsToStone = false;

        public Settings Settings { get; set; }

        public AI(Texture2D texture,Vector2 position, Settings settings, string name) : base(settings, name)
        {
            if(texture is null)
            {
                throw new Exception("AI was given a null texture during construction.");
            }

            this.Texture = texture;
            this.Position = position;
            Settings = settings;

            //Rotation = (float)((Math.PI*2) * Settings.R.NextDouble());
            Rotation = 0;
            WantedRotation = 0.0f;

            if(Settings.R.Next(0,101) % 2 == 0)
            {
                IdleTurnDirectionActual = 1;
            }
            else
            {
                IdleTurnDirectionActual = -1;
            }

            CurrentObjective = new IdleMode(this);

            ScaleMove = settings.ScaleSpeed;

        } 

        public override void Update(GameTime GT)
        {
            base.Update(GT);

            //if our current objective is null...and there are no further objectives
            //enter idle mode
            if (CurrentObjective == null && Objectives.Count == 0)
            {
                //incomplete, to be set to idle mode...
                CurrentObjective = new IdleMode(this);
                Throttle = 0.0f;
            }
            else if (CurrentObjective != null && CurrentObjective.Completed() == true)
            {
                if (Objectives.Count >= 1)
                {
                    Throttle = 0.0f;
                    CurrentObjective = Objectives[0];
                    Objectives.RemoveAt(0);
                }
                else
                {
                    //incomplete, to be set to idle mode...
                    CurrentObjective = new IdleMode(this);
                    Throttle = 0.0f;
                }
            }
            

            //fulfill our current objective
            CurrentObjective.Update(GT);

            ScaleToOne();

            //syntax, should go last
            Position += Velocity * (float)GT.ElapsedGameTime.TotalSeconds;
            RotationLogic(); //turn

            if (m_scaleLocked == false)
            {
                if (WantedScale > Settings.MaxScaleReset)
                {
                    WantedScale = 0.0f;
                }

                if (Scale > Settings.MaxScale)
                {
                    if (Settings.LockOffScale)
                    {
                        m_scaleLocked = true;

                        WantedScale = Settings.MaxScale;
                    }

                    Scale = Settings.MaxScale;
                }
            }
        }

        /// <summary>
        /// Adjusts scale if it is not scaled at 1.0f
        /// </summary>
        public void ScaleToOne()
        {
            if (m_scaleLocked) return; //abort

            if(Scale > WantedScale)
            {
                Scale -= ScaleMove;
            }
            else if (Scale < WantedScale)
            {
                Scale += ScaleMove;
            }

            if(Math.Abs(WantedScale-Scale) < ScaleMove)
            {
                Scale = WantedScale;
            }

        }

        public void Pop()
        {
            if (Settings.PixelBehavior == PopcornPixelBehavior.InstantPop)
            {
                float volume = Settings.Volume;
                float AverageVolume = Settings.AverageVolume;
                //Objectives.Add(new MoveToPosition(Settings.GetRandomNearbyPosition(Position, (int)(Settings.PopcornExplosionRadius * (volume / AverageVolume))), 12, this));
                //move back
                //Objectives.Add(new MoveToPosition(Position, 12, this,Settings));
                //Scale = (Settings.PopcornPopExplosionModifier * (volume / AverageVolume));
                Scale = 0.5f;
                WantedScale = 1.0f;
            }
        }

        /// <summary>
        /// Moves forward at the current throttle level.
        /// </summary>
        public virtual void MoveForward()
        {
            Velocity.X = (float)Math.Cos(Rotation) * Speed * Throttle;
            Velocity.Y = (float)Math.Sin(Rotation) * Speed * Throttle;
        }

        public void RotationLogic()
        {
            //fix rotation overflow...
            Rotation = (float)(Rotation % (Math.PI * 2));

            //if we're close enough anyway...
            if(Math.Abs(WantedRotation - Rotation) <= TurnRate)
            {
                this.Rotation = WantedRotation;
            }
            //we're too far away...
            else
            {
                if(WantedRotation <= Rotation && Math.Abs(WantedRotation - Rotation) < (float)Math.PI)
                {
                    Rotation += TurnRate;
                }
                else
                {
                    Rotation -= TurnRate;
                }
            }

        }
        
        /// <summary>
        /// Returns whether or not this object is facing a coordinate (looking towards it)
        /// </summary>
        /// <param name="Place"></param>
        /// <returns></returns>
        public bool IsFacing(Vector2 Place)
        {
            float AngleToPos = AngleToPosition(this.Position, Place);
            if (Math.Abs(Rotation - AngleToPos) < this.TurnRate)
            {
                return true;
            }

            return false;
        }

    }
}
