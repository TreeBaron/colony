﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sanctuary
{
    abstract class Entity
    {
        public abstract void Update(GameTime GT);

        public abstract void Draw(SpriteBatch SB);

    }
}
