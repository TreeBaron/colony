﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sanctuary
{
    class Resource : PhysicalObject
    {
        float RotationAmount = 0.1f;

        public Resource(Texture2D texture,Vector2 position,Settings settings,string name) : base(settings, name)
        {
            this.Texture = texture;
            this.Position = position;
            this.Scale = 1.0f;
            this.Rotation = 0.0f;

            this.RotationAmount = Utility.GetDegrees(1);
        }

        public override void Update(GameTime GT)
        {
            base.Update(GT);
            this.UpdateCollisionBox();
            this.Rotation += RotationAmount;
        }
    }
}
