﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Sanctuary
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public double VerticalSelector = 0;

        Camera MainCamera;
        Vector2 CameraPosition = new Vector2(0, 0);

        List<PhysicalObject> Queue = new List<PhysicalObject>();
        List<PhysicalObject> GameObjects = new List<PhysicalObject>();

        //audio stuff
        MMDeviceEnumerator Enumerator;
        MMDevice AudioDevice;
        List<float> TotalFrequencyVolumes = new List<float>(4);
        List<float> AverageFrequencyVolumes = new List<float>(4);

        int Ticks = 0;

        public Settings Settings;
        List<PhysicalObject> Backgrounds = new List<PhysicalObject>();

        private string Folder;
        private List<string> Files = new List<string>();
        private List<string> UsedFiles = new List<string>();
        private string CurrentFile;
        private Task LoadNextFile;

        private AudioMaster AudioMaster;

        private List<PhysicalObject> NextQueue = new List<PhysicalObject>();
        private PhysicalObject NextBackground;
        private PhysicalObject PreviousBackground;
        private bool LoadingDone = false;

        private PhysicalObject Background = null;

        public Game1(ModeNames.Mode settingSelection,int audioIndex)
        {

            for (int i = 0; i < 4; i++)
            {
                TotalFrequencyVolumes.Add(0);
                AverageFrequencyVolumes.Add(0);
            }

            switch (settingSelection)
            {
                case ModeNames.Mode.Bass:
                    Settings = SettingsFactory.GetBassSettings();
                    break;
                case ModeNames.Mode.Rainshow:
                    Settings = SettingsFactory.GetRainShow();
                    break;
                case ModeNames.Mode.Psycho:
                    Settings = SettingsFactory.GetDrugsanitySettings();
                    break;
                case ModeNames.Mode.Nightcore:
                    Settings = SettingsFactory.GetNightCoreSettings();
                    break;
                default:
                    throw new Exception("Invalid setting selection.");
            }

            //syntax
            Settings.SelectedAudioIndex = audioIndex;

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Settings.ScreenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            Settings.ScreenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferWidth = Settings.ScreenWidth;// - 100;
            graphics.PreferredBackBufferHeight = Settings.ScreenHeight;// - 100;
            CameraPosition.X += Settings.ScreenWidth / 2;
            CameraPosition.Y += Settings.ScreenHeight / 2;
            graphics.IsFullScreen = Settings.FullScreen;

            //Settings.PlacementBoundaryMins = new Vector2(Settings.ScreenWidth,0);
            //Settings.PlacementBoundariesMax = new Vector2(Settings.ScreenWidth * 4,Settings.ScreenHeight);
            //Settings.PlacementBoundaryMins = new Vector2(Settings.ScreenWidth, 0);
            //Settings.PlacementBoundariesMax = new Vector2((int)(Settings.ScreenWidth*1.1), Settings.ScreenHeight);
            //from center...
            //Settings.PlacementBoundaryMins = new Vector2(0, 0);
            //Settings.PlacementBoundariesMax = new Vector2(Settings.ScreenWidth,Settings.ScreenHeight);
        }

        public float GetVolume()
        {
            return AudioDevice.AudioMeterInformation.MasterPeakValue;
        }

        public void GetAverageFrequencyVolumes(List<int> frequencies)
        {
            Ticks++;
            for (int i = 0; i < frequencies.Count; i++)
            {
                TotalFrequencyVolumes[i] += frequencies[i];

                // do not divide by zero
                if(TotalFrequencyVolumes[i] > 0 && Ticks > 0)
                {
                    AverageFrequencyVolumes[i] = TotalFrequencyVolumes[i] / Ticks;
                }
            }
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            MainCamera = new Camera(GraphicsDevice.Viewport);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Settings.GD = graphics.GraphicsDevice;

            string Filepath = GetFilePathFromUser();
            Folder = Path.GetDirectoryName(Filepath);
            //add jpg files
            List<string> files = new List<string>(Directory.GetFiles(Folder, "*.jpg"));
            //add png files
            files.AddRange(new List<string>(Directory.GetFiles(Folder, "*.png")));
            Files.AddRange(files);
            CurrentFile = Filepath;
            UsedFiles.Add(CurrentFile);
            Files.Remove(CurrentFile);

            //sort files alphabetically so user can control layers
            if (Settings.Mode == ModeNames.Mode.Rainshow)
            {
                Files = Shuffle(Files);
            }
            else
            {
                Files.Sort();
            }

            Action action = () => 
            { 
                LoadImage(CurrentFile);
            };
            LoadNextFile = new Task(action);
            // start loading our image
            LoadNextFile.Start();
            //load our image...
            LoadNextFile.Wait();

            //insert background now
            if (Settings.BackgroundFirst)
            {
                GameObjects.Add(NextBackground);
                Backgrounds.Add(NextBackground);
            }

            //audio stuff
            Enumerator = new MMDeviceEnumerator();
            AudioDevice = Enumerator.GetDefaultAudioEndpoint(DataFlow.Render, Role.Console);

            AudioMaster = new AudioMaster(Settings);
        }

        private List<string> Shuffle(List<string> items)
        {
            List<string> shuffled = new List<string>();

            for(int i = 0; i < items.Count; i++)
            {
                shuffled.Add(items[Settings.R.Next(0, items.Count)]);
            }

            return shuffled;
        }

        private void LoadImage(string filepath)
        {
            Bitmap Reggie = (Bitmap)Bitmap.FromFile(filepath);
            Reggie = ResizeBitmap(Reggie, Settings.ScreenWidth, Settings.ScreenHeight);

            //Reggie = TextureGenerator.TextureGen.ConvertBitmapToBlackAndWhite(Reggie);
            List<PhysicalObject> queue = GenerateBricks(Reggie);

            //generate image for backdrop...
            PhysicalObject background = new PhysicalObject(Settings,"Background");
            background.Position.X += Settings.ScreenWidth/2;
            background.Position.Y += Settings.ScreenHeight / 2;
            background.DrawBox = new Microsoft.Xna.Framework.Rectangle(0, 0, Settings.ScreenWidth, Settings.ScreenHeight);
            background.UseDrawBox = false;

            Bitmap backgroundImage = (Bitmap)Bitmap.FromFile(filepath);

            //resize to correct resolution
            backgroundImage = new Bitmap(backgroundImage, new Size(Settings.ScreenWidth, Settings.ScreenHeight));

            if (Settings.BlackAndWhite)
            {
                backgroundImage = TextureGenerator.TextureGen.ConvertBitmapToBlackAndWhite(backgroundImage);
            }

            background.Texture = TextureGenerator.TextureGen.ConvertBitmapToTexture(backgroundImage, Settings);

            Background = background;
            PreviousBackground = NextBackground;
            NextBackground = background;
            NextQueue.AddRange(queue);
            LoadNextFile = null;
            LoadingDone = true;
        }

        /// <summary>
        /// Retrieves the location of an image file from the user
        /// </summary>
        /// <returns>A string representing an image file path</returns>
        private string GetFilePathFromUser()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            ofd.Filter = "jpg files (*.jpg)|*.jpg|png files (*.png)|*.png";
            ofd.RestoreDirectory = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //get the file path
                string filepath = ofd.FileName;
                return filepath;
            }

            throw new Exception("User Aborted Operation.");
        }

        private List<PhysicalObject> GenerateBricks(Bitmap Reggie)
        {
            List<PhysicalObject> queue = new List<PhysicalObject>();
            int RemainderOffsetX = (Reggie.Width % Settings.TextureBrickWidth);
            int RemainderOffsetY = (Reggie.Height % Settings.TextureBrickHeight);
            int EvenCheckerX = 0;
            for (int y = 0; y < Reggie.Height; y += Settings.TextureBrickHeight) 
            {
                EvenCheckerX++;
                for (int x = 0; x < Reggie.Width; x += Settings.TextureBrickWidth)
                {
                    if (x + Settings.TextureBrickWidth <= Reggie.Width && y + Settings.TextureBrickHeight <= Reggie.Height)
                    {
                        Bitmap TextureAsBitmap = null;
                        switch (Settings.SliceMode)
                        {
                            case SliceMode.Circle:
                                TextureAsBitmap = TextureGenerator.TextureGen.GetTextureCircle(new Microsoft.Xna.Framework.Rectangle(x, y, Settings.TextureBrickWidth, Settings.TextureBrickHeight), Reggie);
                                break;
                            case SliceMode.CircleSingleColor:
                                TextureAsBitmap = TextureGenerator.TextureGen.GetTextureCircleSingleColor(new Microsoft.Xna.Framework.Rectangle(x, y, Settings.TextureBrickWidth, Settings.TextureBrickHeight), Reggie);
                                break;
                            case SliceMode.Square:
                                TextureAsBitmap = TextureGenerator.TextureGen.GetTextureSquare(new Microsoft.Xna.Framework.Rectangle(x, y, Settings.TextureBrickWidth, Settings.TextureBrickHeight), Reggie);
                                break;
                            case SliceMode.CircleFaded:
                                TextureAsBitmap = TextureGenerator.TextureGen.GetTextureCircleFaded(new Microsoft.Xna.Framework.Rectangle(x, y, Settings.TextureBrickWidth, Settings.TextureBrickHeight), Reggie);
                                break;
                            case SliceMode.SquareFaded:
                                TextureAsBitmap = TextureGenerator.TextureGen.GetTextureSquareFaded(new Microsoft.Xna.Framework.Rectangle(x, y, Settings.TextureBrickWidth, Settings.TextureBrickHeight), Reggie);
                                break;
                        }
                        
                        if (TextureAsBitmap != null)
                        {
                            Texture2D textureboi = TextureGenerator.TextureGen.ConvertBitmapToTexture(TextureAsBitmap, Settings);
                            AI Bot = new AI(textureboi, new Vector2(x + Settings.TextureBrickWidth / 2, y + Settings.TextureBrickHeight / 2), Settings, "Brick");
                            //Bot.Objectives.Add(new MoveToPosition(new Vector2(x + Settings.TextureBrickWidth / 2, y + Settings.TextureBrickHeight / 2), Settings.TextureBrickWidth, Bot, Settings));
                            Bot.Scale = 1.0f;
                            Bot.WantedScale = 1.0f;
                            queue.Add(Bot);
                        }
                    }
                }
            }
            return queue;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            List<int> frequencies = AudioMaster.GetFrequencies();

            //abort until we get sound
            if (frequencies.Count == 0) return;

            //I'm so sorry :(
            Settings.Volume = GetVolume();
            GetAverageFrequencyVolumes(frequencies);

            if (Settings.BaseCameraScale == 0.0f)
            {
                Settings.BaseCameraScale = MainCamera.scale;
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == Microsoft.Xna.Framework.Input.ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
                Exit();

            float volume = AudioDevice.AudioMeterInformation.MasterPeakValue;
            

            Settings.ScreenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            Settings.ScreenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;

            ModeNames.Mode mode = Settings.Mode;
            switch (mode)
            {
                case ModeNames.Mode.Bass:
                    PixelExplosion(frequencies[0],AverageFrequencyVolumes[0]);
                    PopcornExplosion(frequencies[2], AverageFrequencyVolumes[2]);
                    ChangeBackgroundColor(frequencies[1], AverageFrequencyVolumes[1], 3.0f);
                    RotatingBackgrounds(frequencies[0], AverageFrequencyVolumes[0], 3.0f);
                    ShakingBackgrounds(frequencies[0], AverageFrequencyVolumes[0], 3.0f);
                    PixelForcedRotation(frequencies[0], AverageFrequencyVolumes[0]);
                    //dequeue bits (this is important)
                    DequeueBits(frequencies[0], AverageFrequencyVolumes[0],1.1f);
                    break;
                case ModeNames.Mode.Rainshow:
                    PixelLineDrawHorizontal(frequencies[3], AverageFrequencyVolumes[3]);
                    PixelLineDrawVertical(frequencies[2], AverageFrequencyVolumes[2]);
                    ChangeBackgroundColor(frequencies[0], AverageFrequencyVolumes[0],1.0f);
                    ShakingBackgrounds(frequencies[1], AverageFrequencyVolumes[1], 0.25f);
                    //dequeue bits (this is important)
                    DequeueBits(frequencies[3], AverageFrequencyVolumes[3],0.75f);
                    break;
                case ModeNames.Mode.Psycho:
                    PixelLineDrawVertical(frequencies[0], AverageFrequencyVolumes[0]);
                    PixelLineDrawHorizontal(frequencies[1], AverageFrequencyVolumes[1],0.5f);
                    PixelLineDrawHorizontal(frequencies[2], AverageFrequencyVolumes[2], 0.5f);
                    PixelExplosion(frequencies[3], AverageFrequencyVolumes[3],0.001f);
                    ChangeBackgroundColor(frequencies[0], AverageFrequencyVolumes[0], 0.5f);
                    //dequeue bits (this is important)
                    DequeueBits(frequencies[0], AverageFrequencyVolumes[0], 1.0f);
                    MainCamera.Rotation += ((float) Math.PI/ 18000f) * (frequencies[3] / AverageFrequencyVolumes[3]);
                    break;
                case ModeNames.Mode.Nightcore:
                    CenterBurst(frequencies[3], AverageFrequencyVolumes[3]);
                    ChangeBackgroundColor(frequencies[2], AverageFrequencyVolumes[2],2.0f);
                    PopcornExplosion(frequencies[1], AverageFrequencyVolumes[1]);
                    ShakingBackgrounds(frequencies[3], AverageFrequencyVolumes[3], 5.0f);
                    //dequeue bits (this is important)
                    DequeueBits(frequencies[3], AverageFrequencyVolumes[3]);
                    break;
            }

            //really important we update the camera...
            MainCamera.Update(gameTime, ref CameraPosition);
            List<PhysicalObject> WalkingDead = new List<PhysicalObject>();
            foreach (PhysicalObject PO in GameObjects)
            {
                PO.Update(gameTime);

                if (PO.Alive == false)
                {
                    WalkingDead.Add(PO);
                }
            }
            //clear out dead objects
            foreach (PhysicalObject PO in WalkingDead)
            {
                GameObjects.Remove(PO);
            }
            WalkingDead.Clear();

            int CameraSpeed = 10;
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.W))
            {
                CameraPosition.Y -= CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.A))
            {
                CameraPosition.X -= CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.S))
            {
                CameraPosition.Y += CameraSpeed;
            }
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.D))
            {
                CameraPosition.X += CameraSpeed;
            }


           
            base.Update(gameTime);

        }

        private void ChangeBackgroundColor(float volume, float averageVolume, float multiplier)
        {
            if (volume / averageVolume > 1.0 || Settings.R.Next(0,3601) == 5)
            {
                int amount = (int)(5 * multiplier);
                int r = Settings.BackgroundColor.R + Settings.R.Next(-1*amount, amount+1);
                int g = Settings.BackgroundColor.G + Settings.R.Next(-1*amount, amount+1);
                int b = Settings.BackgroundColor.B + Settings.R.Next(-1*amount, amount+1);
                Settings.BackgroundColor = new Microsoft.Xna.Framework.Color(r, g, b);
            }
        }

        private void RotatingBackgrounds(float volume, float averageVolume, float multiplier)
        {
            if (volume / averageVolume > 1.0)
            {
                foreach (PhysicalObject po in Backgrounds)
                {
                    int amount = (int)(volume - averageVolume);
                    amount = (int)(amount * multiplier);
                    amount = Math.Max(1, amount);
                    po.Rotation += ((float)Math.PI/18000.0f) * Settings.R.Next(-1 * amount, amount + 1);
                    
                    //don't rotate too far off normal (0.0174 == 1 degree aproximately)
                    if(Math.Abs(po.Rotation - 0) > 0.0174f * 1)
                    {
                        po.Rotation = 0;
                    }

                }
            }
        }

        private void ShakingBackgrounds(float volume, float averageVolume, float multiplier)
        {
            if (volume / averageVolume > 1.0)
            {
                foreach (PhysicalObject po in Backgrounds)
                {
                    int amount = (int)(volume - averageVolume);
                    amount = (int)(amount * multiplier);
                    amount = Math.Max(1, amount);

                    po.Scale += 0.00001f * Settings.R.Next(-1 * amount, amount + 1);

                    //don't scale too far off base
                    if (Math.Abs(po.Scale - 1.0) > (0.01f*multiplier))
                    {
                        po.Scale = 1.0f;
                    }
                }
            }
        }


        private void PixelForcedOrder(float volume, float averageVolume)
        {
            Vector2 Point = new Vector2(Settings.R.Next(MainCamera.view.X, MainCamera.view.Width), Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));
            double Radius = 100 * (volume / averageVolume);

            //woah pretty loud makes the other pixels "jump"
            if (volume > averageVolume * 1.0)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (Vector2.Distance(PO.Position, Point) < Radius && PO.GetType() == typeof(AI))
                    {
                        AI DatBoi = (AI)PO;
                        DatBoi.WantedRotation = 0.0f;
                        DatBoi.Rotation = 0.0f;
                    }
                }
            }

        }

        private void PixelForcedRotation(float volume, float averageVolume)
        {
            //abort!
            if (Queue.Count == 0) return;

            Vector2 point = new Vector2(Settings.R.Next(MainCamera.view.X, MainCamera.view.Width), Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));
            double radius = 1500 * (volume / averageVolume);
            double intensity = volume / averageVolume;

            //woah pretty loud makes the other pixels "jump"
            if (volume / averageVolume > 1.0)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (Vector2.Distance(PO.Position, point) < radius && PO.GetType() == typeof(AI))
                    {
                        AI DatBoi = (AI)PO;
                        DatBoi.Rotation += Utility.GetDegrees(1) * (float)intensity;
                        DatBoi.WantedRotation = 0.0f;
                    }
                }
            }

        }

        private void PixelForcedShrink(float volume, float averageVolume)
        {

            Vector2 Point = new Vector2(Settings.R.Next(MainCamera.view.X, MainCamera.view.Width), Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));
            double Radius = Settings.ForceShrinkRadius * Math.Pow((volume / averageVolume), 2);

            //woah pretty loud makes the other pixels "jump"
            if (volume > averageVolume * 1.0)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (Vector2.Distance(PO.Position, Point) < Radius && PO.GetType() == typeof(AI))
                    {
                        AI DatBoi = (AI)PO;
                        DatBoi.Scale = 0.3f;
                    }
                }
            }
            else if (volume > averageVolume * 1.1)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (Vector2.Distance(PO.Position, Point) < Radius && PO.GetType() == typeof(AI))
                    {
                        AI DatBoi = (AI)PO;
                        DatBoi.Scale = 0.2f;
                    }
                }
            }
            else if (volume > averageVolume * 1.2)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (Vector2.Distance(PO.Position, Point) < Radius && PO.GetType() == typeof(AI))
                    {
                        AI DatBoi = (AI)PO;
                        DatBoi.Scale = 0.1f;
                    }
                }
            }
            else if (volume > averageVolume * 1.5)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (Vector2.Distance(PO.Position, Point) < Radius && PO.GetType() == typeof(AI))
                    {
                        AI DatBoi = (AI)PO;
                        DatBoi.Scale = 0.0f;
                    }
                }
            }
        }

        private void PixelExplosion(float volume, float averageVolume, float wantedScale = 1.0f)
        {
            if (Queue.Count == 0) return;

            Vector2 Point = new Vector2(Settings.R.Next(MainCamera.view.X, MainCamera.view.Width), Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));
            double powerScale = Math.Pow(volume / averageVolume, 2);
            double weakerPowerScale = Math.Pow(volume / averageVolume, 1);
            double Radius = 100f * powerScale;

            if (volume > averageVolume * 0.5f)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (PO.GetType() == typeof(AI) && Vector2.Distance(Point, PO.Position) < Radius)
                    {
                        AI DatBoi = (AI)PO;
                        if (DatBoi.Objectives.Count == 0 && DatBoi.CurrentObjective.Completed() == true && DatBoi.CanModify == true)
                        {
                            //move to position
                            DatBoi.Objectives.Add(new MoveToPosition(Utility.GetRandomNearbyPosition(DatBoi.Position, (int)(Settings.ExplosionRadius * weakerPowerScale), Settings), 12, DatBoi, Settings));
                            //move back
                            DatBoi.Objectives.Add(new MoveToPosition(PO.Position, 8, DatBoi, Settings));

                            DatBoi.Scale = Settings.ExplosionScaleModifier * (float)powerScale;
                            DatBoi.WantedScale = wantedScale;
                        }
                    }
                }
            }

                        
        }

        private void PopcornExplosion(float volume, float averageVolume)
        {
            //abort if transitioning to picture...
            if (Queue.Count == 0) return;

            Vector2 Point = new Vector2(Settings.R.Next(MainCamera.view.X, MainCamera.view.Width), Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));
            double Radius = (Settings.ExplosionRadius * (volume / averageVolume));//best 20

            if (volume > averageVolume * 0.15)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    if (PO.GetType() == typeof(AI) && Vector2.Distance(Point, PO.Position) < Radius)
                    {
                        AI DatBoi = (AI)PO;
                        if (DatBoi.Objectives.Count == 0 && DatBoi.CurrentObjective.Completed() == true && DatBoi.CanModify == true)
                        {
                            DatBoi.Pop();
                        }
                    }
                }
            }

        }

        private void PixelLineDrawVertical(float volume, float averageVolume, float wantedScale = 2.0f)
        {
            if (volume > averageVolume * 0.1)
            {
                Vector2 Point = new Vector2((float)VerticalSelector, Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));
                VerticalSelector += (volume/averageVolume) * 3;

                if (VerticalSelector > Settings.ScreenWidth)
                {
                    VerticalSelector = 0;
                }

                foreach (PhysicalObject PO in GameObjects)
                {
                    //if we're in X distance only...
                    if (PO.GetType() == typeof(AI) && Math.Abs(Point.X - PO.Position.X) < Settings.TextureBrickWidth)
                    {
                        AI DatBoi = (AI)PO;
                        if (DatBoi.Objectives.Count == 0 && DatBoi.CurrentObjective.Completed() == true)
                        {
                            //move to position
                            DatBoi.Objectives.Add(new MoveToPosition(Utility.GetRandomNearbyPosition(DatBoi.Position, (int)(Settings.ExplosionRadius * (volume / averageVolume)), Settings), 12, DatBoi, Settings));
                            //move back
                            DatBoi.Objectives.Add(new MoveToPosition(PO.Position, 8, DatBoi, Settings));

                            DatBoi.Scale = 3.0f;
                            DatBoi.WantedScale = 1.0f;
                        }
                    }
                }
            }

        }

        private void PixelLineDrawHorizontal(float volume, float averageVolume, float wantedScale = 1.0f)
        {
            Vector2 Point = new Vector2(Settings.R.Next(MainCamera.view.X, MainCamera.view.Width), Settings.R.Next(MainCamera.view.Y, MainCamera.view.Height));

            if (volume > averageVolume * 1.1)
            {
                foreach (PhysicalObject PO in GameObjects)
                {
                    //if we're in X distance only...
                    if (PO.GetType() == typeof(AI) && Math.Abs(Point.Y - PO.Position.Y) < 5)
                    {
                        AI DatBoi = (AI)PO;
                        if (DatBoi.Objectives.Count == 0 && DatBoi.CurrentObjective.Completed() == true)
                        {
                            //move to position
                            //DatBoi.Objectives.Add(new MoveToPosition(DatBoi.Position + new Vector2(0, (Settings.ScreenHeight / 8) * (volume / averageVolume)), 12, DatBoi, Settings));

                            //move back
                            DatBoi.Objectives.Add(new MoveToPosition(PO.Position, 8, DatBoi, Settings));

                            DatBoi.Position.Y -= Settings.ScreenHeight - Settings.R.Next(0,Settings.ScreenHeight);

                            DatBoi.Scale = 0.15f;
                            DatBoi.WantedScale = wantedScale;
                        }
                    }
                }
            }

        }

        private void CenterBurst(float volume, float averageVolume)
        {
            Vector2 Point = new Vector2(Settings.ScreenWidth / 2, Settings.ScreenHeight / 2);
            double power = Math.Pow((volume / averageVolume),6);
            float Radius = (Settings.CenterBurstRadius * (float)power);

            foreach (PhysicalObject PO in GameObjects)
            {
                //if we're in X distance only...
                if (PO.GetType() == typeof(AI) && Vector2.Distance(Point, PO.Position) < Radius)
                {
                    AI DatBoi = (AI)PO;

                    if (DatBoi.Objectives.Count == 0 && DatBoi.CurrentObjective.Completed() == true)
                    {
                        //was previously...this line below
                        //DatBoi.Scale = (0.25f * (volume / AverageVolume));

                        //move to position
                        //DatBoi.Objectives.Add(new MoveToPosition(DatBoi.Position + new Vector2(0, (Settings.ScreenHeight / 8) * (volume / AverageVolume)), 12, DatBoi, Settings));
                        //move back
                        //DatBoi.Objectives.Add(new MoveToPosition(PO.Position, 8, DatBoi, Settings));
                        var scaleValue = (Settings.ExplosionScaleModifier * (volume / averageVolume));
                        var distance = Vector2.Distance(Point, PO.Position);
                        var scaler = 1.0f - (distance / Radius);
                        //offset by distance
                        scaleValue *= Math.Min(1.0f, scaler);
                        DatBoi.WantedScale = scaleValue;
                        //DatBoi.WantedScale += Settings.GrowthRate;
                    }
                }

            }


        }

        private void DequeueBits(float volume, float averageVolume, float threshold = 1.0f)
        {
            if(Files.Count == 0)
            {
                Files.AddRange(UsedFiles);
            }

            //start loading asap
            if (LoadingDone == true && Files.Count >= 1 && Queue.Count == 0)
            {

                //cover up the adding of our background image with an explosion! :D
                PixelExplosion(150, 1);

                //order all pixels to shrink and dissappear
                foreach (PhysicalObject po in GameObjects)
                {
                    if (po is AI)
                    {
                        AI bot = (AI)po;
                        bot.WantedScale = 0.001f;
                    }
                }

                //set flag
                LoadingDone = false;
                //select next image...
                CurrentFile = Files[0];
                UsedFiles.Add(CurrentFile);
                Files.RemoveAt(0);
                //switch images
                Action action = () => { LoadImage(CurrentFile); };
                LoadNextFile = new Task(action, TaskCreationOptions.LongRunning);
                // start loading our image
                LoadNextFile.Start();

                GameObjects.Clear();
                GameObjects.AddRange(Backgrounds);
                Queue.AddRange(NextQueue);
                NextQueue.Clear();
                //if there is a previous background loaded
                //and we can have at least 1 background
                if (PreviousBackground != null && Settings.MaxBackgroundCount > 0)
                {
                    //add the previous background
                    Backgrounds.Add(PreviousBackground);
                    GameObjects.Add(PreviousBackground);

                    //remove overflowing backgrounds based on limit
                    while (Backgrounds.Count > Settings.MaxBackgroundCount)
                    {
                        GameObjects.Remove(Backgrounds[0]);
                        Backgrounds.RemoveAt(0);
                    }
                }

                //insert background now
                if (Settings.BackgroundFirst)
                {
                    GameObjects.Insert(0, Background);
                    GameObjects.Add(Background);
                    //add so it shakes and dances
                    Backgrounds.Add(Background);
                }

            }

            //If there are queue items and the music is reasonably loud
            if (Queue.Count >= 1 && (volume / averageVolume) > threshold)
            {
                //select a random item to dequeue
                int selection = Settings.R.Next(0, Queue.Count);

                //if its rainshow select the first item to dequeue
                if (Settings.Mode == ModeNames.Mode.Rainshow)
                {
                    selection = Queue.Count-1;
                }

                //set starting scale to 3.0 so it pops out at the user when it appears
                Queue[selection].Scale = 3.0f;
                GameObjects.Add(Queue[selection]);
                Queue.RemoveAt(selection);
            }

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Settings.BackgroundColor);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, null, null, null, null, MainCamera.transform);
            foreach (PhysicalObject PO in GameObjects)
            {
                PO.Draw(spriteBatch);
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public static Bitmap ResizeBitmap(Bitmap BP, int width, int height)
        {
            Bitmap output = new Bitmap(width, height);
            using (Graphics graphics = Graphics.FromImage(output))
            {
                graphics.DrawImage(BP, 0, 0, width, height);
            }
            return output;
        }
    }
}
