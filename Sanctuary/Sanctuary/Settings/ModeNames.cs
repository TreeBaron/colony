﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sanctuary
{
    public enum SliceMode
    {
        Circle,
        CircleSingleColor,
        Square,
        CircleFaded,
        SquareFaded
    }

    public static class ModeNames
    {
        public enum Mode
        {
            Bass,
            Rainshow,
            Nightcore,
            Psycho
        }

        public static Dictionary<string, Mode> Modes = new Dictionary<string, Mode>()
        {
            { "Bass (Not Chill) - For Bassy Songs" , Mode.Bass},
            { "Rainshow (Chill) - Works best on full images and relatively gentle songs.", Mode.Rainshow},
            { "Nightcore (Not Chill) - Tailored for Nightcore Music", Mode.Nightcore},
            { "Psycho (Chill) - For Radiohead, Tame Impala, Childish Gambino and More...", Mode.Psycho}
        };
    }
}
