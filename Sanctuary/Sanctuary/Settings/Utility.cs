﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Sanctuary
{
    public static class Utility
    {
        public static float GetDegrees(int amount = 1)
        {
            return (float)(amount * (Math.PI / 180.0));
        }

        public static Vector2 GetRandomPosition(Vector2 CameraPosition, Settings settings)
        {
            Vector2 Position = new Vector2(settings.R.Next((int)CameraPosition.X - settings.ScreenWidth / 2, (int)CameraPosition.X + settings.ScreenWidth / 2),
                settings.R.Next((int)CameraPosition.Y - settings.ScreenHeight / 2, (int)CameraPosition.Y + settings.ScreenHeight / 2));
            return Position;
        }

        public static Vector2 GetRandomPositionAtBottomOfScreen(Vector2 CameraPosition,Settings settings)
        {
            Vector2 Position = new Vector2(settings.R.Next((int)CameraPosition.X - settings.ScreenWidth / 2, (int)CameraPosition.X + settings.ScreenWidth / 2),
                CameraPosition.Y + settings.ScreenHeight);
            return Position;
        }

        public static Vector2 GetRandomNearbyPosition(Vector2 CurrentPosition, int Radius,Settings settings)
        {
            return new Vector2(CurrentPosition.X + settings.R.Next(-Radius, Radius), CurrentPosition.Y + settings.R.Next(-Radius, Radius));
        }
    }
}
