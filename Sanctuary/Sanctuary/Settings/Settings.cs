﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Sanctuary
{
    public class Settings
    {
        public GraphicsDevice GD { get; set; }

        public Random R { get; set; } =  new Random(DateTime.Now.Millisecond);

        /// <summary>
        /// The amount of time in seconds that elapses before
        /// the mode is switched when mode is set to cycle.
        /// </summary>
        public int CycleModeInterval = 60;

        public ModeNames.Mode Mode { get; set; } = ModeNames.Mode.Bass;

        public float GrowthRate = 0.05f;

        public SliceMode SliceMode { get; set; } = SliceMode.SquareFaded;

        public  Vector2 PlacementBoundaryMins { get; set; } 
        public  Vector2 PlacementBoundariesMax { get; set; } 
        
        public bool LockOffScale { get; set; }

        public  int ScreenWidth { get; set; }
        public  int ScreenHeight { get; set; }

        public int SelectedAudioIndex { get; set; } = 19;

        public  bool BlackAndWhite { get; set; }

        public bool Layer { get; set; }

        public  bool ShowBackgroundImage { get; set; }

        public  bool FullScreen { get; set; }

        public  Color BackgroundColor { get; set; }

        public  float MaxScale { get; set; }

        public  int ExplosionRadius { get; set; }
        public  float ExplosionScaleModifier { get; set; }

        public  int ForceShrinkRadius { get; set; }

        public  float CenterBurstRadius { get; set; }

        public int TextureBrickWidth { get; set; }
        public int TextureBrickHeight { get; set; }

        public bool PixelsRotate { get; set; }

        public bool BackgroundFirst { get; set; } = false;

        public PopcornPixelBehavior PixelBehavior { get; set; }

        public float PopcornExplosionRadius { get; set; } 
        public float PopcornPopExplosionModifier { get; set; }

        public float Volume { get; set; }
        public float AverageVolume { get; set; }

        public  float BaseCameraScale { get; set; }
        public float MaxScaleReset { get; set; }
        public bool AddPixelsInstantly { get; set; }
        public float ScaleSpeed { get; set; }
        public int MaxBackgroundCount { get; internal set; }
    }
}
