﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace Sanctuary
{
    public static class SettingsFactory
    {
        /// <summary>
        /// Returns the default settings.
        /// </summary>
        /// <returns>Settings</returns>
        public static Settings GetDefaultSettings()
        {
            Settings settings = new Settings();

            settings.GD = null;
            settings.R = new Random(DateTime.Now.Millisecond);
            settings.GrowthRate = 0.15f;
            settings.MaxScaleReset = 1.0f;
            settings.LockOffScale = false;
            settings.MaxScale = 15.25f;
            settings.Mode = ModeNames.Mode.Bass;
            settings.PlacementBoundaryMins = new Vector2(0, 0);
            settings.PlacementBoundariesMax = new Vector2(4000, 4000);
            settings.ScreenWidth = 0;
            settings.ScreenHeight = 0;
            settings.BlackAndWhite = true;
            settings.ShowBackgroundImage = true;
            settings.FullScreen = true; // set to true when releasing
            settings.BackgroundColor = Color.Black;
            settings.ExplosionRadius = 400;
            settings.ExplosionScaleModifier = 1.0f;
            settings.ForceShrinkRadius = 100;
            settings.CenterBurstRadius = 850;
            settings.TextureBrickHeight = 24;
            settings.TextureBrickWidth = 24;
            settings.PixelsRotate = false;
            settings.PixelBehavior = PopcornPixelBehavior.InstantPop;
            settings.PopcornExplosionRadius = 850f;
            settings.PopcornPopExplosionModifier = 5.0f;
            settings.Volume = 0.0f;
            settings.AverageVolume = 0.0f;
            settings.BaseCameraScale = 0.0f;
            settings.AddPixelsInstantly = false;
            settings.ScaleSpeed = 0.15f;
            settings.Layer = true;
            settings.MaxBackgroundCount = 25;
            settings.SliceMode = SliceMode.SquareFaded;
            settings.BackgroundFirst = false;

            return settings;
        }

        internal static Settings GetNightCoreSettings()
        {
            Settings settings = GetDefaultSettings();
            settings.Mode = ModeNames.Mode.Nightcore;
            settings.MaxScaleReset = 2.0f;
            settings.MaxScale = 1.25f;
            settings.ScaleSpeed = 0.05f;
            settings.LockOffScale = false;
            settings.ShowBackgroundImage = true;
            settings.Layer = true;
            settings.ExplosionScaleModifier = 1.0f;
            settings.ExplosionRadius = 10;
            settings.BlackAndWhite = false;
            settings.BackgroundColor = Color.HotPink;
            settings.MaxBackgroundCount = 30;
            settings.PixelsRotate = true;
            return settings;
        }

        internal static Settings GetRainShow()
        {
            Settings settings = GetDefaultSettings();
            settings.Mode = ModeNames.Mode.Rainshow;
            settings.MaxScaleReset = 3.0f;
            settings.MaxScale = 1.25f;
            settings.ScaleSpeed = 0.005f;
            settings.LockOffScale = false;
            settings.ShowBackgroundImage = true;
            settings.Layer = true;
            settings.ExplosionScaleModifier = 1.0f;
            settings.ExplosionRadius = 10;
            settings.BlackAndWhite = true;
            settings.BackgroundColor = Color.LightBlue;
            settings.MaxBackgroundCount = 1;
            settings.PixelsRotate = true;
            settings.SliceMode = SliceMode.CircleFaded;
            settings.GrowthRate = 0.0005f;
            settings.BackgroundFirst = true;
            return settings;
        }

        /// <summary>
        /// Returns chaos settings.
        /// </summary>
        /// <returns></returns>
        public static Settings GetDrugsanitySettings()
        {
            Settings settings = GetDefaultSettings();
            settings.Mode = ModeNames.Mode.Psycho;
            settings.MaxScaleReset = 2.25f;
            settings.MaxScale = 2.25f;
            settings.ScaleSpeed = 0.05f;
            settings.LockOffScale = false;
            settings.ShowBackgroundImage = true;
            settings.Layer = true;
            settings.BlackAndWhite = false;
            settings.BackgroundColor = new Color(48, 25, 52);
            settings.MaxBackgroundCount = 0;
            settings.PixelsRotate = true;
            settings.SliceMode = SliceMode.CircleSingleColor;
            settings.ExplosionScaleModifier = 25.0f;
            settings.ExplosionRadius = 150;
            settings.PopcornExplosionRadius = 350;
            settings.PopcornPopExplosionModifier = 5.0f;
            return settings;
        }

        /// <summary>
        /// Gets the center burst settings.
        /// </summary>
        /// <returns></returns>
        public static Settings GetBassSettings()
        {
            Settings settings = GetDefaultSettings();
            settings.Mode = ModeNames.Mode.Bass;
            settings.MaxScaleReset = 3.25f;
            settings.MaxScale = 3.25f;
            settings.ScaleSpeed = 0.075f;
            settings.LockOffScale = false;
            settings.ShowBackgroundImage = false;
            settings.Layer = true;
            settings.ExplosionScaleModifier = 10.0f;
            settings.ExplosionRadius = 50;
            settings.BlackAndWhite = false;
            settings.BackgroundColor = Color.Turquoise;
            settings.MaxBackgroundCount = 30;
            settings.PixelsRotate = true;
            settings.MaxBackgroundCount = 0;
            settings.BackgroundFirst = true;
            return settings;
        }

    }
}
